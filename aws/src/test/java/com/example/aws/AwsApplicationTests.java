package com.example.aws;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.example.aws.config.AmazonS3ClientConfig;
import com.example.aws.config.AmazonS3ContainerInitializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = AmazonS3ClientConfig.class)
@ContextConfiguration(initializers = AmazonS3ContainerInitializer.class)
class AwsApplicationTests {

    private static final String BUCKET_NAME = "my-bucket";

    @Autowired
    private AmazonS3 amazonS3Client;

    @BeforeEach
    void setUp() {
        amazonS3Client.createBucket(BUCKET_NAME);
    }

    @Test
    void amazonS3() {
        String bucketLocation = amazonS3Client.getBucketLocation(BUCKET_NAME);

        assertEquals(Regions.US_WEST_1.getName(), bucketLocation);
    }
}
