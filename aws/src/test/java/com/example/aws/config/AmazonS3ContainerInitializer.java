package com.example.aws.config;

import com.amazonaws.regions.Regions;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.lang.NonNull;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.utility.DockerImageName;

import static org.testcontainers.containers.localstack.LocalStackContainer.Service.S3;

public class AmazonS3ContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    //    API_GATEWAY, EC2, KINESIS, DYNAMODB, DYNAMODB_STREAMS, ELASTICSEARCH, S3, FIREHOSE, LAMBDA, SNS, SQS,
//    REDSHIFT, ELASTICSEARCH_SERVICE, SES, ROUTE53, CLOUDFORMATION, CLOUDWATCH, SSM, SECRETSMANAGER,
//    STEPFUNCTIONS, CLOUDWATCHLOGS, STS, IAM, KMS

    private static final DockerImageName LOCALSTACK_IMAGE = DockerImageName.parse("localstack/localstack:0.11.3");
    public static final LocalStackContainer AWS_S3_CONTAINER = new LocalStackContainer(LOCALSTACK_IMAGE)
        .withEnv("DEFAULT_REGION", Regions.US_WEST_1.getName())
        .withServices(S3);

    static {
        AWS_S3_CONTAINER.start();
    }

    @Override
    public void initialize(@NonNull ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
            "access.key=" + AWS_S3_CONTAINER.getAccessKey(),
            "secret.key=" + AWS_S3_CONTAINER.getSecretKey(),
            "endpoint.override=" + AWS_S3_CONTAINER.getEndpointOverride(S3).toString(),
            "region=" + AWS_S3_CONTAINER.getRegion()
        ).applyTo(applicationContext);
    }
}
