package com.example.postgres;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SecondPostgresTest extends PostgresBaseTest {

    @Test
    void first_test() {
        assertEquals(5432, port);
    }

    @Test
    void second_test() {
        assertEquals(5432, port);
    }
}
