package com.example.postgres;

import com.example.postgres.config.PostgresInitializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(initializers = PostgresInitializer.class)
abstract class PostgresBaseTest {

    @Value("${datasource.port}")
    protected int port;
}
