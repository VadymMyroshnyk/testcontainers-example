package com.example.custom;

import com.example.custom.config.RedisClientConfig;
import com.example.custom.config.RedisContainerInitializer;
import com.example.custom.model.User;
import com.example.custom.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ContextConfiguration(classes = {CustomApplication.class, RedisClientConfig.class},
    initializers = RedisContainerInitializer.class)
class CustomApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    void redis() {
        User user = createUser();
        userRepository.save(user);

        Iterable<User> users = userRepository.findAll();
        User next = users.iterator().next();

        assertThat(next)
            .usingRecursiveComparison()
            .ignoringFields("id")
            .isEqualTo(user);
    }

    private User createUser() {
        User user = new User();
        user.setName("username");

        return user;
    }
}
