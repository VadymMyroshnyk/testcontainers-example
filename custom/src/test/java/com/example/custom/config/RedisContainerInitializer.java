package com.example.custom.config;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.lang.NonNull;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.DockerImageName;

public class RedisContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final DockerImageName REDIS_IMAGE = DockerImageName.parse("redis:7.0.4-alpine");
    private static final GenericContainer<?> REDIS = new GenericContainer<>(REDIS_IMAGE)
        .withExposedPorts(6379);

    static {
        REDIS.start();
    }

    @Override
    public void initialize(@NonNull ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
            "redis.port=" + REDIS.getFirstMappedPort(),
            "redis.host=" + REDIS.getHost()
        ).applyTo(applicationContext);
    }
}
