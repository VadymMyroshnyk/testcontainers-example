package com.example.postgres;

import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
abstract class PostgresBaseTest {

    @Container
    public final PostgreSQLContainer<?> container = new PostgreSQLContainer<>("postgres:11")
        .withDatabaseName("db-name")
        .withUsername("my_user")
        .withPassword("my_password");
}
