package com.example.dockercompose.config;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.lang.NonNull;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;

import java.io.File;

public class RedisContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    public static final int REDIS_PORT = 6379;
    private static final int ELASTICSEARCH_PORT = 9200;

    public static final DockerComposeContainer<?> REDIS =
        new DockerComposeContainer<>(new File("src/test/resources/compose-test.yml"))
            .withExposedService("redis_1", REDIS_PORT)
            .withExposedService("elasticsearch_1", ELASTICSEARCH_PORT);

    static {
        REDIS.waitingFor("redis_1", Wait.forListeningPort());
        REDIS.start();
    }

    @Override
    public void initialize(@NonNull ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
            "redis.port=" + REDIS.getServicePort("redis_1", REDIS_PORT),
            "redis.host=" + REDIS.getServiceHost("redis_1", REDIS_PORT)
        ).applyTo(applicationContext);
    }
}
