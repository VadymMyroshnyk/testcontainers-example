package com.example.dockercompose;

import com.example.dockercompose.config.RedisClientConfig;
import com.example.dockercompose.config.RedisContainerInitializer;
import com.example.dockercompose.model.User;
import com.example.dockercompose.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ContextConfiguration(classes = {DockerComposeApplication.class, RedisClientConfig.class},
    initializers = RedisContainerInitializer.class)
class DockerComposeApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    void docker() {
        User user = createUser();
        userRepository.save(user);

        Iterable<User> users = userRepository.findAll();
        User next = users.iterator().next();

        assertThat(next)
            .usingRecursiveComparison()
            .ignoringFields("id")
            .isEqualTo(user);
    }

    private User createUser() {
        User user = new User();
        user.setName("username");

        return user;
    }
}
