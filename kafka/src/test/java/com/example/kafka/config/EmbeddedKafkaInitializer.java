package com.example.kafka.config;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.lang.NonNull;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

public class EmbeddedKafkaInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private final KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"));

    @Override
    public void initialize(@NonNull ConfigurableApplicationContext applicationContext) {
        kafka.start();

        TestPropertyValues.of(
            "kafka.host=" + String.format("%s:%d", kafka.getHost(), kafka.getFirstMappedPort())
        ).applyTo(applicationContext);
    }
}
