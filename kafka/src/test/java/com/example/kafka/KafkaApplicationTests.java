package com.example.kafka;

import com.example.kafka.config.EmbeddedKafkaInitializer;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.time.Duration;
import java.util.Properties;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ContextConfiguration(initializers = EmbeddedKafkaInitializer.class)
class KafkaApplicationTests {

    private static final String EVENT = "\"code\":\"event_code\"";
    private static final String TOPIC = "my_topic";

    @Value("${kafka.host}")
    private String kafkaHost;

    @AfterEach
    void setUp() {
        System.out.println(kafkaHost);
    }

    @Test
    void kafka() {
        Properties properties = getProperties();

        pushEventToKafka(properties);

        String eventFromKafka = getEventFromKafka(properties);

        assertEquals(EVENT, eventFromKafka);
    }

    private void pushEventToKafka(Properties properties) {
        Producer<String, String> producer = new KafkaProducer<>(properties);
        ProducerRecord<String, String> rec = new ProducerRecord<>(TOPIC, EVENT);
        producer.send(rec);
    }

    private String getEventFromKafka(Properties properties) {
        Consumer<Long, String> consumer = new KafkaConsumer<>(properties);
        TopicPartition topicPartition = new TopicPartition(TOPIC, 0);
        consumer.assign(singletonList(topicPartition));

        ConsumerRecords<Long, String> consumedRecords = consumer.poll(Duration.ofSeconds(3));
        Iterable<ConsumerRecord<Long, String>> records = consumedRecords.records(topicPartition);
        ConsumerRecord<Long, String> consumerRecord = records.iterator().next();

        return consumerRecord.value();
    }

    private Properties getProperties() {
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaHost);
        configProperties.put(ProducerConfig.CLIENT_ID_CONFIG, "IntegrationTest");
        configProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "consumerId");
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        configProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        configProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        configProperties.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, "1000");
        configProperties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "40");
        configProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        return configProperties;
    }
}
