package com.example.postgres;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FirstPostgresTests extends PostgresBaseTest {

    @Value("${datasource.port}")
    private int port;

    @Test
    void first_test() {
        assertEquals(5432, port);
    }

    @Test
    void second_test() {
        assertEquals(5432, port);
    }
}
